"use strict";

if (!window._App) {
	var _App = {};
	throw new Error('app.js is not initialized');
}

/**
 * On-load initializations
 */
$(function () {

	_App.fn.init();

    _App.el.header = $('#header');
    _App.el.wrapper = $('#wrapper');

	_App.fn.setEvents();
	_App.fn.setContextEvents();

	// Init slick
    (function () {

        var sliders = $('.to-slick');
        if (!sliders.length) {
            return;
        }

        _App.vr.plugins.load(
            'slick', function () {
                sliders.slick({
                    dots: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 1,
                    adaptiveHeight: true
                });
            }
        );

    })();

});

/**
 * Set events for non-dynamic elements
 */
_App.fn.setEvents = function () {
};

/**
 * Set events for elements in context
 * @param {jQuery} [context]
 */
_App.fn.setContextEvents = function (context) {
	if (!context) {
		context = _App.el.body;
	}

	// AJAX forms
    (function () {

        var forms = $('.ajax-form', context);
        if (!forms.length) {
            return;
        }
        forms.ajaxForm();

    })();

    // GoTo buttons
    (function () {

        // Scroll down buttons
        $('.action-scroll', context).click(function () {
            var body = $('html, body');
            switch ($(this).attr('rel')) {
                case 'top':
                    body.animate(
                        {
                            'scrollTop': 0
                        },
                        400
                    );
                    break;
                case 'down':
                    var box = $(this).closest('.cover, .screen-block, .cblock');
                    if (box.length) {
                        body.animate(
                            {
                                'scrollTop': box.offset().top + box.outerHeight()
                            },
                            400
                        );
                    }
                    break;
            }
        });

    })();
};

/**
 * Show confirm window and go to URL if confirmed
 * @param {string} url
 * @param {string} [text]
 * @param {string} [title]
 * @returns {EspressioModalConfirm}
 */
_App.fn.confirmAndGo = function (url, text, title) {
	if (!url) {
		throw new Error('No URL defined');
	}
	return _App.fn.confirmAndDo(
		function () {
			location.href = url;
		},
		text,
		title
	);
};

/**
 * Show confirm window and execute callback if confirmed
 * @param {function} callback
 * @param {string} [text]
 * @param {string} [title]
 * @returns {EspressioModalConfirm}
 */
_App.fn.confirmAndDo = function (callback, text, title) {
	if (typeof callback !== 'function') {
		throw new Error('Callback is not defined');
	}
	if (!text) {
		text = 'Are you sure?';
	}
	if (!title) {
		title = 'Confirm action';
	}

	var confirm = new EspressioModalConfirm(
		text,
		title,
		{
			askValue: false
		}
	);

	confirm.form().on('confirm', function() {
		callback();
	});

	return confirm;
};

/**
 * Default AJAX form
 * @param {jQuery} form
 * @param {function} [onSend]
 */
_App.fn.ajaxForm = function (form, onSend) {
    if (!(form instanceof jQuery)) {
        throw new Error('Form is not a valid jQuery instance');
    }
    if (onSend && typeof onSend !== 'function') {
        throw new Error('onSend is not a valid function instance');
    }

    var inputs = $(':input[name]', form);

    form.submit(function (e) {
        e.preventDefault && e.preventDefault();

        var xhr = _App.fn.xhr.post(
            form.attr('action'),
            form.serialize(),
            function () {
                var success = $('.success', form);
                if (success.length) {
                    success.removeClass('hidden');
                    $('fieldset', form).not('.success').remove();
                } else {
                    location.reload();
                }
            },
            function (xhr) {
                if (xhr.status >= 500) {
                    var error = $('.error', form);
                    if (error.length) {
                        error.removeClass('hidden');
                    } else {
                        _App.fn.alert('Failed to send form: ' + xhr.responseText);
                    }
                } else {
                    var fields = xhr.responseText;
                    fields = JSON.parse(fields);
                    $.each(fields, function (i, name) {
                        inputs.filter('[name="' + name + '"]').addClass('error');
                    });
                }
            }
        );

        if (onSend) {
            onSend(xhr);
        }

        return false;
    });

    inputs.keydown(function () {
        $(this).removeClass('error');
    });
};

/**
 * Scroll body to specified element
 *
 * @param {string|jQuery} element
 * @param {int} [animate_time]
 * @param {function} [callback]
 * @returns {Boolean}
 */
_App.fn.scrollToElement = function (element, animate_time, callback) {

    if (!element) {
        return false;
    }

    if (!(element instanceof jQuery)) {
        element = $(element);
    }

    if (typeof animate_time === 'undefined') {
        animate_time = 300;
    }

    $('body, html').animate(
        {
            scrollTop: element.offset().top
        },
        animate_time,
        callback
    );

    return true;
};

/**
 * jQuery plugins
 */
(function ($) {

    /**
     * Add new file button plugin
     * @returns {*}
     */
    jQuery.fn.ajaxForm = function () {
        var make = function () {
            _App.fn.ajaxForm($(this));
        };
        return this.each(make);
    };

})(jQuery);