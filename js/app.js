"use strict";

/**
 * Console enable
 * @type {boolean}
 */
var CONS = !!window.console && true;

/**
 * Main data object (_App)
 * @type object
 * @private
 */
var _App = {

    // Functions
    fn: {},

    // Variables
    vr: {},

    // Elements
    el: {},

    // Constants
    define: {
        CLASS_ACTIVE: 'active',
        CLASS_EMPTY: 'empty',
        CLASS_DISABLED: 'disabled',
        CLASS_HIDDEN: 'hidden'
    }
};


/**
 * Initializations
 */
_App.fn.init = function () {

    _App.el.body = $('body');

    _App.vr.loader = new _App.Loader();
    _App.vr.plugins = new _App.PluginManager();

    /**
     * Register onbeforeunload event
     */
    _App.fn.windowUnloadMessages.registerEvent();

    // Loader auto show/hide
    (function () {

        _App.vr.loader.hide();

        _App.fn.windowUnloadMessages.add(
            function () {
                _App.vr.loader.show(true);
            },
            'show-loader'
        );

        // Avoid show loader after mailto: link click
        $(document).on('click', 'a[href*="mailto:"], a[href*="tel:"]', function () {
            _App.fn.windowUnloadMessages.remove('show-loader');
        });

        // Avoid showing loader on come back from history
        if (_App.fn.browser.history()) {
            $(window).bind('popstate', function () {
                _App.vr.loader.hide();
            });
        }

    })();

    // Espressio modal default values
    if ('EspressioModalDefaults' in window) {
        /**
         * Set Espressio Modal options defaults
         * @see {EspressioModalDefaults} in esp-modal.js
         */
        EspressioModalDefaults.removeOnHide = true;
        EspressioModalDefaults.closeIcon = true;
        EspressioModalDefaults.buttonsDefaultClass = 'btn';
        EspressioModalDefaults.btnApplyClass = 'btn-ok';
        EspressioModalDefaults.btnCancelClass = 'btn-warn';
        EspressioModalDefaults.btnApplyText = 'OK';
        EspressioModalDefaults.btnCancelText = 'Cancel';
    }
};

// region FUNCTIONS

/**
 * Browser supports
 * @type {Object}
 */
_App.fn.browser = {

    /**
     * Cached values
     * @type Object
     */
    _values: {
        dnd: null,
        history: null,
        mobile: null,
        touch: null,
        flash: null,
        ie: null
    },

    /**
     * Check drag'n'drop support
     * @returns {boolean}
     */
    dragAndDrop: function () {
        if (this._values.dnd === null) {
            var div = document.createElement('div');
            this._values.dnd = ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div);
            if (!this._values.dnd && CONS) {
                console.warn('There is no drag&drop support');
            }
        }

        return this._values.dnd;
    },

    /**
     * Check history support
     * @returns {boolean}
     */
    history: function () {
        if (this._values.history === null) {
            this._values.history = !!(window.history && history.pushState);
            if (!this._values.history && CONS) {
                console.warn('There is no history support');
            }
        }

        return this._values.history;
    },

    /**
     * Detect mobile user agent
     * @returns {boolean}
     */
    detectMob: function () {
        if (this._values.mobile !== null) {
            return !!this._values.mobile;
        }
        var plugin = (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        );
        this._values.mobile = !!plugin;
        return this._values.mobile;
    },

    /**
     * Detect touch device
     * @returns {boolean}
     */
    detectTouch: function () {
        if (this._values.touch !== null) {
            return !!this._values.touch;
        }
        var plugin = "ontouchstart" in window
            || window.DocumentTouch && document instanceof DocumentTouch
            || !!document.createTouch
            || (('onmsgesturechange' in window) && !!window.navigator.maxTouchPoints);
        this._values.touch = !!plugin;
        return this._values.touch;
    }
};

/**
 * Random data
 * @type object
 */
_App.fn.rand = {

    /**
     * Unique ID current value
     */
    _uniqueId: (Math.floor(Math.random() * 26) + Date.now()),

    /**
     * Get unique ID
     * @returns {int}
     */
    uniqueId: function () {
        this._uniqueId++;
        return this._uniqueId;
    }
};

/**
 * Get URL prepended with base path
 * @param url
 * @returns {string}
 */
_App.fn.url = function (url) {
    return _appData.basePath + url;
};

/**
 * Stylized error alert
 * @param {string} msg
 * @param {string} [title]
 * @param {string|object|jQuery} [button]
 * @param {int} [maxLength]
 * @returns {EspressioModalAlert}
 */
_App.fn.alert = function (msg, title, button, maxLength) {
    if (typeof title === 'undefined') {
        title = 'Error';
    }
    if (typeof maxLength === 'undefined') {
        maxLength = 500;
    }

    if (maxLength) {
        msg = msg.slice(0, maxLength) + (msg > maxLength ? '...' : '');
    }

    var modal = null;

    if ('EspressioModalAlert' in window) {
        modal = new EspressioModalAlert(msg, title, button);
    } else {
        alert(msg);
    }

    CONS && console.error(msg);

    return modal;
};

/**
 * AJAX queries processing
 * @type object
 */
_App.fn.xhr = {

    /**
     * Default jqXHR fail handler
     * @param xhr
     * @param [callbackOrError]
     * @param [force401Alert]
     */
    fail: function (xhr, callbackOrError, force401Alert) {
        if (force401Alert === undefined) {
            force401Alert = true;
        }
        xhr.fail(function (xhr, textStatus, errorThrown) {
            var is401 = xhr.status === 401;
            if (callbackOrError && !(is401 && force401Alert)) {
                if (typeof callbackOrError === 'string') {
                    _App.fn.alert(callbackOrError);
                } else {
                    callbackOrError(xhr, textStatus, errorThrown);
                }
            } else {
                if (is401) {
                    _App.fn.alert(
                        'Your authorization session is over. Please, reload the page.',
                        'Unauthorized',
                        {
                            text: 'Reload',
                            onclick: function () {
                                location.reload();
                            }
                        }
                    );
                } else {
                    _App.fn.alert(xhr.responseText);
                }
            }
        });
    },

    /**
     * Default POST query
     * @param url
     * @param data
     * @param callback
     * @param [failCallbackOrError]
     * @param [withLoader]
     * @param [responseType]
     * @returns {Deferred}
     */
    post: function (url, data, callback, failCallbackOrError, withLoader, responseType) {
        if (typeof withLoader === 'undefined') {
            withLoader = true;
        } else {
            withLoader = !!withLoader;
        }

        if (typeof responseType === 'undefined') {
            responseType = 'json';
        }

        if (withLoader) {
            var loader = new _App.Loader();
            loader.show();
        }

        var xhr = $.post(
            url,
            data,
            callback,
            responseType
        );

        if (withLoader) {
            xhr.always(function () {
                loader.hide();
            });
        }

        this.fail(xhr, failCallbackOrError);

        return xhr;
    },

    /**
     * Check if authorization session is active
     * @param {boolean} [async]
     * @returns {boolean}
     */
    checkAuth: function (async) {
        if (async === undefined) {
            async = false;
        }
        var xhr = $.ajax(
            {
                async: async,
                dataType: 'json',
                url: _App.fn.url('auth/check-auth'),
                type: 'POST'
            }
        );
        this.fail(xhr);

        var isAuth = false;
        xhr.done(function (res) {
            isAuth = !!res;
        });

        return (!async ? isAuth : xhr);
    }
};

/**
 * Cookie processing
 * @type {Object}
 */
_App.fn.cookie = {

    /**
     * Get Cookie Value
     * @param name
     * @returns mixed
     */
    get: function (name) {
        var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([.$?*|{}()\[\]\\\/+^])/g, '\\$1') + "=([^;]*)"));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    },

    /**
     * Set Cookie Value
     * @param name
     * @param value
     * @param options
     * @returns void
     */
    set: function (name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires === "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        $.each(options, function (propName, propValue) {
            updatedCookie += "; " + propName;
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        });

        document.cookie = updatedCookie;
    }
};

/**
 * WindowUnload messages
 * @type object
 */
_App.fn.windowUnloadMessages = {

    /**
     * List of messages in format {<messageId>: '<messageText>', ...}
     * If event registered and list is not empty,
     * alert will be shown if user tries to leave the page.
     */
    messages: {},

    /**
     * Register unload message.
     * If id is not defined, unique ID will be rendered.
     * Returns message id.
     * @param {string|function} message
     * @param {string} id
     * @returns {string}
     */
    add: function (message, id) {
        if (!id) {
            id = 'm' + _App.fn.rand.uniqueId();
        }
        this.messages[id] = message;

        return id;
    },

    /**
     * Remove message from list
     * @param {string} id
     * @returns {boolean}
     */
    remove: function (id) {
        if (typeof id === 'undefined') {
            throw new Error('Message ID is not defined');
        }

        if (typeof this.messages[id] === 'undefined') {
            return false;
        }

        delete this.messages[id];

        return true;
    },

    /**
     * Render messages list
     * @returns {string|null}
     */
    render: function () {
        var messages = [];

        $.each(this.messages, function (id, message) {
            if (typeof message === 'function') {
                message = message();
            }
            if (message) {
                messages.push(message);
            }
        });

        if (!messages.length) {
            return null;
        }

        return messages.join("\n");
    },

    /**
     * Register window OnBeforeUnload event handler
     */
    registerEvent: function () {

        var instance = this;

        window.onbeforeunload = function (e) {
            var message = instance.render();
            if (!message) {
                return;
            }

            if (typeof e === 'undefined') {
                e = window.event;
            }
            if (e) {
                e.returnValue = message;
            }

            return message;
        };
    }
};

/**
 * Check if element is on the screen
 * @param elem
 * @returns {boolean}
 */
_App.fn.elementIsVisible = function (elem) {
    if (!(elem instanceof jQuery)) {
        throw new Error('Expected jQuery instance, ' + (typeof elem) + ' given');
    }
    if (!elem.is(':visible')) {
        return false;
    }
    var win = $(window);
    var scroll = win.scrollTop();
    var top = elem.offset().top;
    var h = elem.height();
    return (scroll + win.height() >= top && scroll <= top + h);
};

/**
 * Common View
 * @type {object}
 */
_App.fn.view = {};

/**
 * Update view
 * @param {string} url
 * @param {jQuery} [context]
 * @param {function} [onFail]
 * @param {boolean} [withLoader]
 * @return {Deferred}
 */
_App.fn.view.updateView = function (url, context, onFail, withLoader) {
    if (!url) {
        throw new Error('URL is not defined');
    }

    if (!context) {
        context = _App.el.body;
    }

    return _App.fn.xhr.post(
        url,
        {},
        function (data) {
            $.each(data.html, function (selector, content) {
                var el = $(selector, context);
                if (el.length) {
                    el.html(content);
                    _App.fn.setContextEvents(el);
                }
            });
            _App.el.body.trigger('View.Update', url, data, context);
        },
        onFail,
        withLoader
    );
};

// endregion FUNCTIONS

// region PRE-LOADER

/**
 * PreLoader overlay
 * @param {jQuery} [loader] Loader element object
 * @returns {_App.Loader}
 * @constructor
 */
_App.Loader = function (loader) {

    /**
     * Current instance
     * @type {_App.Loader}
     */
    var instance = this;

    if (!loader) {
        loader = $('#loader-overlay');
    } else if (!(loader instanceof jQuery)) {
        throw new Error('Loader param value is not a valid jQuery instance');
    }

    /**
     * Show loader timeout to short loader show
     * @type {int}
     */
    var timeout;

    /**
     * Get loader element object
     * @returns {jQuery}
     */
    this.elem = function () {
        return loader;
    };

    /**
     * Show loader
     * @param {boolean} [noTimeout]
     * @returns {_App.Loader}
     */
    this.show = function (noTimeout) {
        var loader = instance.elem();
        if (loader) {
            var show = function () {
                if (!loader.hasClass(_App.define.CLASS_ACTIVE)) {
                    loader.addClass(_App.define.CLASS_ACTIVE);
                    loader.show();
                }
            };
            if (!noTimeout) {
                timeout = setTimeout(show, 200);
            } else {
                show();
            }
        }
        return instance;
    };

    /**
     * Hide loader
     * @returns {_App.Loader}
     */
    this.hide = function () {
        var loader = instance.elem();
        if (loader) {
            if (timeout) {
                clearTimeout(timeout);
            }
            if (loader.hasClass(_App.define.CLASS_ACTIVE)) {
                loader.removeClass(_App.define.CLASS_ACTIVE);
                loader.hide();
            }
        }
        return instance;
    };

    return this;
};

// endregion PRE-LOADER

// region PLUGIN MANAGER

/**
 * JS plugins manager
 * @constructor
 */
_App.PluginManager = function () {

    /**
     * Current instance
     * @type {_App.PluginManager}
     */
    var instance = this;

    /**
     * List of loaded packages
     * @type {Array}
     */
    var loaded = [];

    /**
     * List of currently loading packages
     * @type {Object}
     */
    var loading = {};

    /**
     * Load package file and execute callback
     * @param {string|Array} pkg
     * @param {function} [callback]
     * @returns {Deferred}
     */
    this.load = function (pkg, callback) {
        if (callback && typeof callback !== 'function') {
            callback = null;
            CONS && console.warn('Callback is not a function, ' + typeof callback + ' given');
        }

        if (typeof pkg === 'string') {
            pkg = [pkg];
        }

        var promises = [];

        $.each(pkg, function (i, pkg) {
            var url = _App.fn.url('js/plugins/' + pkg + '.min.js');

            if (instance.isLoaded(pkg)) {
                promises.push({});
                CONS && console.log('Plugins package already loaded:', url);
                return;
            }

            if (loading[pkg]) {
                promises.push(loading[pkg]);
                CONS && console.log('Plugins package is loading:', url);
                return;
            }

            var xhr = $.getScript(
                url,
                function () {
                    loaded.push(pkg);
                    CONS && console.log('Loaded plugins package:', url);
                }
            );

            xhr.fail(function () {
                throw new Error('Failed to load plugins package:' + url);
            });

            loading[pkg] = xhr;

            xhr.always(function () {
                loading[pkg] = null;
                delete loading[pkg];
            });

            promises.push(xhr);
        });

        var res = $.when.apply($, promises);

        if (callback) {
            res.then(callback);
        }

        return res;
    };

    /**
     * Is package loaded?
     * @param {string} pkg
     * @returns {boolean}
     */
    this.isLoaded = function (pkg) {
        return (loaded.indexOf(pkg) >= 0);
    };
};

// endregion PLUGIN MANAGER