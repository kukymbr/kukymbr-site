#!/bin/sh

## Variables

# Abs file path
SELF=$(readlink -f $0);

# Input params source file
INPUT_PARAMS="${SELF%%.sh}.conf";

# Project directory
BASEDIR=$(dirname $(dirname $SELF))"/";

# OK Label
LABEL_OK="\033[0;32m[OK]\033[0m";

# FAIL Label
LABEL_FAIL="\033[0;31m[FAIL]\033[0m";

# WARN Label
LABEL_WARN="\033[0;33m[WARN]\033[0m";

# Path to README.md file
README="$BASEDIR""README.md";



## Check input

if [ ! -f $INPUT_PARAMS ]
then
	echo "$LABEL_FAIL\tNo input params file ($INPUT_PARAMS) found";
	exit 1;
fi;

# Include input params file
. $INPUT_PARAMS;



## Run

echo "\n$(tput bold)CHECK INSTALL";
echo "Basedir: $BASEDIR";
echo "Config file: $INPUT_PARAMS$(tput sgr0)";

echo "\n----- CHECK EXIST -----";

if [ -n "$DIRS_CHECK_EXIST" ]
then
	for item in $DIRS_CHECK_EXIST
	do
		dir="$BASEDIR$item";
		if [ -d $dir ]
		then
			echo "$LABEL_OK\t$dir";
		else
			echo "$LABEL_FAIL\t$dir";
		fi;
	done;
else
	echo "$LABEL_OK\tNothing to check";
fi; # if [ -n $DIRS_CHECK_EXIST ]



echo "\n----- CREATE IF NOT EXIST -----";

if [ -n "$DIRS_CREATE" ]
then
	for item in $DIRS_CREATE
	do
		dir="$BASEDIR$item";
		if [ ! -d $dir ]
		then
			mkdir $dir && echo "$LABEL_OK\t$dir" || echo "$LABEL_FAIL\t$dir";
		else
			echo "$LABEL_OK\t$dir";
		fi;
	done;
else
	echo "$LABEL_OK\tNothing to check";
fi; # if [ -n $DIRS_CREATE ]



echo "\n----- CHECK WRITABLE -----";

if [ -n "$DIRS_WRITABLE" ]
then
	for item in $DIRS_WRITABLE
	do
		recursive="${item#*=}";
		dir=$BASEDIR"${item%%=*}";

		# Set 777 permissions

		if [ -d $dir ]
		then
			perm=$(stat -c "%a" $dir);
			if [ "$perm" != "777" -o $recursive -eq 1 ]
			then
				if [ $recursive -eq 0 ]
				then
					chmod 0777 $dir;
				else
					chmod -R 0777 $dir;
				fi;
				perm=$(stat -c "%a" $dir);
				if [ "$perm" = "777" ]
				then
					echo "$LABEL_OK\t$dir";
				else
					echo "$LABEL_FAIL\t$dir";
				fi;
			else
				echo "$LABEL_OK\t$dir";
			fi;
		fi;

	done;

else
	echo "$LABEL_OK\tNothing to check";
fi; # if [ -n $DIRS_WRITABLE ]



echo "\n----- PHP MODULES -----";

if [ -n "$PHP_MODULES" ]
then
	for item in $PHP_MODULES
	do
		required="${item#*=}";
		module="${item%%=*}";

		moduleAvailable=$(php -m | grep "^$module$");

		if [ -n "$moduleAvailable" ]
		then
			echo "$LABEL_OK\t$module";
		else
			if [ $required -eq 1 ]
			then
				label="$LABEL_FAIL";
			else
				label="$LABEL_WARN";
			fi;
			echo "$label\t$module";
		fi;
	done;
else
	echo "$LABEL_OK\tNothing to check";
fi; # if [ -n $PHP_MODULES ]



echo "\n----- CRONTAB -----";

if [ -n "$CRONTAB_JOBS" ]
then
	for item in $CRONTAB_JOBS
	do
		target="/etc/cron.${item#*=}/";
		scriptName="${item%%=*}";
		script=$(dirname $SELF)"/$scriptName";

		if [ -d $target ]
		then
			if [ -f $script ]
			then
				cp "$script" "$target";
				res=$?;
				if [ $res != 0 ]
				then
					echo "$LABEL_FAIL\tFailed to copy $script to $target";
				else
					echo "$LABEL_OK\tCopied $script to $target";

					chmod u+x "$target$scriptName";
					res=$?;
					if [ $res != 0 ]
					then
						echo "$LABEL_WARN\tFailed to change mode for $target$scriptName";
					fi;
				fi;
			else
				echo "$LABEL_FAIL\t$script does not exist";
			fi;
		else
			echo "$LABEL_FAIL\tDirectory $target does not exist";
		fi;

	done;
else
	echo "$LABEL_OK\tNothing to check";
fi; # if [ -n $CRONTAB_JOBS ]



if [ -f $README ]
then
	echo "\n----- README -----";
	echo "Read $README for install notes."
fi;



echo "\n$(tput bold)----- DONE -----$(tput sgr0)\n";
exit 0;