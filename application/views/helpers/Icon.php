<?php

/**
 * Icon helper
 */ 
class Helper_Icon extends Core_View_Helper_Abstract
{
	/**
	 * Render SVG icon element
	 *
	 * @param string $name
	 * @param int|null $size
	 * @param bool $asImg
	 * @return Core_Dom_Element
	 * @throws Core_Exception
	 */
	public function icon(string $name = null, int $size = null, $asImg = false)
	{
		if ($name === null) {
			$name = 'default';
		}

		if ($size === null) {
			$size = 16;
		}

		$path = 'img/icons/' . $name . '.svg';
		$url = $this->view->url($path);

		if ($path = Core_App::path('../' . $path, false, true)) {
			$url .= '?' . filemtime($path);
		}

		$icon = new Core_Dom_Element(
			'span',
			array(
				'class' => 'ico'
			)
		);

		if (!$asImg) {
			$icon->getAttributes()->addClass('ico-' . $size)
				 ->set('style', 'background-image:url(\'' . $url . '\')');
		} else {
			$icon->getAttributes()->addClass('ico-img');
			$icon->appendContent(
				'<img src="' . $url . '" width="' . $size . '" />'
			);
		}

		return $icon;
	}
}