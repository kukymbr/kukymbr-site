<?php

/**
 * CasesItem helper
 */
class Helper_CasesItem extends Core_View_Helper_Abstract
{
	/**
	 * Render cases item
	 *
	 * @param string $name
	 * @param string $label
	 * @param string|null $text
	 * @return Core_Dom_Element
	 * @throws Core_Exception
	 */
	public function casesItem(string $name, string $label, string $text = null)
	{
		$item = new Core_Dom_Element(
			'div',
			array(
				'class' => 'item',
			)
		);

		$img = new Core_Dom_Element(
			'img',
			array(
				'class' => 'img',
				'src' => $this->view->url('img/cases/' . $name . '.png')
			)
		);

		$item->appendContent($img);

		$content = new Core_Dom_Element(
			'div',
			array(
				'class' => 'content v-align center'
			)
		);

		$content->appendContent('<h4 class="name">' . $label . '</h4>');

		if ($text !== null) {
			$content->appendContent($text);
		}

		$item->appendContent(
			'<div class="overlay">' .
			$content .
			'<div class="aligner"></div>' .
			'</div>'
		);

		return $item;
	}
}