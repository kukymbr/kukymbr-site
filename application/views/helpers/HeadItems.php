<?php

class Helper_HeadItems extends Core_View_Helper_HeadItems
{
	/**
	 * List of default items
	 * grouped by templates name
	 *
	 * Format examples:
	 * <code>
	 * array(
	 *      'favicon.ico',
	 *		'index.css', 'main.js',
	 *		'http://code.jquery.com/jquery-latest.min.js',
	 *		array('http://api-maps.yandex.ru/2.0-stable/', 'js'),
	 *      array(
	 *          'path'       => 'player.js',
	 *          'type'       => 'js',
	 *          'isFullPath' => false,
	 *          'async'      => true,
	 *          'defer'      => true
	 *      )
	 * );
	 * </code>
	 *
	 * @var array
	 */
	protected $_defaultHeadItems = array(

		'main' => array(
			'style.css',
			'jquery-3.3.1.min.js',
			'app.js',
			'main.js'
		)
	);

	/**
	 * Paths to head files relative to basePath
	 * @var array
	 */
	protected $_paths = array(
		'css' => 'css',
		'js'  => 'js',
		'ico' => 'img'
	);

	/**
	 * List of app config sections to pass to JS data.
	 * Values may be set in path-format, e. g. session/cookie
	 * @var array
	 */
	protected $_shareAppConfSections = array();

	/**
	 * Search for minified items in the same dir
	 * @var bool
	 */
	protected $_minItemsInSameDir = true;
}
