<?php

/**
 * Application config array
 * @return array
 */
return array(

	// App environment mode (develop/production)
	'environment' => 'develop',

	// Default timezone
	'timezone' => 'Europe/Moscow',

	// Base path if http_host is not
	// an application URL
	// Set NULL to get automatic value
	'basePath' => null,



	// Application params (Optional section)
	'app' => array(

		// Application name
		'name' => 'Fp3dWeb Application',

		// Add application name to page title
		'addNameToTitle' => true
	),


	/**
	 * Date & time config
	 * @see Core_DateTime
	 */
	'dateTime' => array(

		// Date string format
		'dateFormat' => 'Y-m-d',

		// Time string format
		'timeFormat' => 'H:i:s',

		// Date&time string format
		'dateTimeFormat' => 'Y-m-d H:i:s',

		// Use empty string if timestamp is zero
		'allowEmptyValue' => true,

		// Time-elapsed string function params
		'elapsed' => array(

			// Render '<N> seconds ago' if diff less than minute
			'withSeconds' => false,

			// If diff more than $threshold seconds, date string will be returned
			'threshold' => false,

			// Add time to date string if $threshold reached
			'withTime' => false
		)
	),



	// DB connection params
	// Required if Core_Db is used in application
	'db' => array(

		// Local DB
		'local' => array(

			// Config name label
			'label' => 'Developer Local Database',

			// Path to config file (relative to application/config dir)
			'path' => 'db/local.php',

			// Hostname or list of hosts where this preset is used.
			// If current host is equal to one of specified there, and no preset
			// is defined in dbconf.php file, this preset will be selected automatically.
			// If there is only one preset in application config it will be used.
			'useIfHost' => false
		)

		// ... some other preset
	),



	// DB profiler params
	'dbProfiler' => array(

		// Enable profiler
		'enable' => false,

		/**
		 * Profiler renderer object or class name
		 * Renderer must implement Core_Db_Profiler_Renderer interface
		 * @see Core_Db_Profiler_Renderer
		 */
		'renderer' => 'Core_Db_Profiler_Renderer_Csv',

		// Renderer config (array or NULL)
		'rendererConfig' => array(

			// Path to report files directory
			'path' => DataStorage::getInstance()->dir('db-profiler'),

			// CSV renderer mode
			'mode' => Core_Db_Profiler_Renderer_Csv::MODE_REQUESTS,

			// Report files lifetime
			// Set NULL to prevent deleting reports
			'fileLifetime' => 3600 * 24 * 7
		)
	),


	// MVC params
	'mvc' => array(

		// Disable default view rendering
		'disableViewOutput' => false,

		// Default controller name
		'defaultControllerName' => 'main',

		// 404 page controller name
		'errorControllerName' => 'error',

		// Actions naming mode (poi/zend)
		'actionsNamingMode' => 'zend',

		// Default layout name
		'defaultLayoutName' => 'main',

		// Allow response throw 'headers already sent' exception
		// If this directive is not set, TRUE will be set by default
		'responseThrowHeadersException' => true,

		// View files (scripts & templates) extension
		'viewFilesExt' => 'phtml',

		// If TRUE view triggers notice if variable is not set
		'viewStrictVars' => false,

		// Remove double spaces, new lines and tabs from output
		'outputRemoveSpaces' => true,

		// Remove html comments from output (<!-- comment -->)
		'outputRemoveComments' => false,

		// Set class name, object (implements Core_View_OutputModifier) or
		// modifiers array in format array($modifier1, $modifier2, ...)
		// or array($modifierClass1 => $modifierConfig1, $modifierClass2 => $modifierConfig2, ...)
		'outputModifier' => null
	),



	// Authorization params
	'auth' => array(

		// Enable authorization (TRUE if directive is not set)
		'enable' => false,

		// Authorization model name
		// It's recommended to use models extends Core_Model_Default_Users
		// Model must implement Core_Auth_Interface
		'modelName' => null,

		// Authorization controller name
		'controllerName' => 'auth',

		// Session key for login storage
		'sessionKey' => 'CoreLibApp_authData',

		// IP compare on session initialization
		'validateIp' => true,

		// User Agent compare on session initialization
		'validateUserAgent' => true,

		// Default access roles
		'defaultRoles' => array(
			Core_Auth::ROLE_ROOT,
			Core_Auth::ROLE_ADMIN,
			Core_Auth::ROLE_EDITOR
		),
	),



	// Fp3d API params
	'fp3dApi' => array(

		// Public API key
		// e. g. Core_App::path('config/ssl/fp3d_public.pem')
		'sslPublicKey' => null,

		// App authorization key, public part (login)
		'accessPublicKey' => null,

		// App authorization key, private part (password)
		'accessPrivateKey' => null,

		// Fp3d API services params
		'services' => array(

			// Fp3dAuth service params
			'fp3dAuth' => array(

				// Enable authorization through Fp3dAuth
				'enable' => false,

				// Server URL
				'serverUrl' => Core_Auth_Fp3d_Server::DEFAULT_API_URL,

				// Success callback URL
				'successUrl' => 'auth/success',

				// Name of the cache instance to store responses
				'cacheName' => Core_App::DEFAULT_CACHE_NAME,
			)
		)
	),



	// Cache configuration sets
	'cache' => array(

		// Default cache instance
		Core_App::DEFAULT_CACHE_NAME => array(

			// Cache adapter name
			'adapter' => Core_Cache::ADAPTER_APCU,

			// Alternative adapter, if main is not available
			'alternativeAdapter' => Core_Cache::ADAPTER_FILE,

			// Cache storage path for file adapter
			'path' => DataStorage::getInstance()->dir('cache') . Core_App::DEFAULT_CACHE_NAME,

			// Cache lifetime
			'lifetime' => 1800,

			// Serialize flag
			// 'serialize' => true,

			// Namespace
			// 'namespace' => 'default',

			// Allow to share cache with other projects
			// 'shared' => true
		),

		// Default Core_Db metadata cache instance
		Core_Db::DEFAULT_META_CACHE_NAME => array(

			// Cache adapter name
			'adapter' => Core_Cache::ADAPTER_APCU,

			// Alternative adapter, if main is not available
			'alternativeAdapter' => Core_Cache::ADAPTER_FILE,

			// Cache storage path (relative to application dir)
			'path' => DataStorage::getInstance()->dir('cache') . Core_Db::DEFAULT_META_CACHE_NAME,

			// Cache lifetime
			'lifetime'	=> 172800
		)
	),



	// Errors processing params
	'errors' => array(

		// Errors handler (string, array or empty)
		// 'handler' => array(Errors_Handler::getInstance(), 'handler'),

		// Errors types to pass to specified handler
		// 'types' => -1,

		// Disable specified handler if request is sent by AJAX
		// 'disableIfAjax' => true
	),



	// Security params
	'security' => array(

		// open_basedir php directive
		// set FALSE to not set open_basedir directive
		'openBasedir' => array(
			Core_App::path('..'),
			sys_get_temp_dir()
		)
	),



	// Session params
	'session' => array(

		// Application session name
		// Set empty for default
		'name' => null,

		// Path to session files storage
		// Set empty for php default
		'savePath' => DataStorage::getInstance()->dir('session'),

		/**
		 * Cookie params to override defaults
		 * Remember the 'path' param is set to basePath by default
		 * @link http://php.net/manual/en/function.session-set-cookie-params.php
		 */
		'cookie' => array(

			// Lifetime
			'lifetime' => 6 * 3600
		),

		// Prolong session cookie at every request,
		// if cookie lifetime > 0
		'prolongAlways' => false,

		/**
		 * Strict session edit mode.
		 * Switch it on to disallow edit $_SESSION directly from code.
		 * @see Core_Session::isStarted()
		 * @see Core_Session::strictEditModePrecondition()
		 */
		'strictEditMode' => false,
	),

	/**
	 * Google proxy config
	 * @see Core_Gprx
	 */
	'gprx' => array(

		// Enable proxy?
		'enable' => true,

		// Proxy host URL
		'host' => Core_Fp3d::API_HOST_GPRX
	)
);