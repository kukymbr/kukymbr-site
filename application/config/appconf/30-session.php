<?php

return array(

	// Session params
	'session' => array(

		// Application session name
		// Set empty for default
		'name' => null,

		// Path to session files storage
		// Set empty for php default
		'savePath' => DataStorage::getInstance()->dir('session'),

		/**
		 * Cookie params to override defaults
		 * Remember the 'path' param is set to basePath by default
		 * @link http://php.net/manual/en/function.session-set-cookie-params.php
		 */
		'cookie' => array(),

		/**
		 * Strict session edit mode.
		 * Switch it on to disallow edit $_SESSION directly from code.
		 * @see Core_Session::isStarted()
		 * @see Core_Session::strictEditModePrecondition()
		 */
		'strictEditMode' => true,
	),
);