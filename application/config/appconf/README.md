# App config extension dir

It is possible to override or extend application config params by adding 
php files returning options array into `application/config/appconf/` directory.
Files will be loaded in alphabetical order 
and options of `application/config/appconf.php` file
will be replaced or extended by specified in these files options.

For example, file `no-cache.php` in this directory disables project caches:

		<?php 
		
		return array(
			'cache' => array(
				Core_App::DEFAULT_CACHE_NAME => array(
					'adapter' => Core_Cache::ADAPTER_NULL
				),
				Core_Db::DEFAULT_META_CACHE_NAME => array(
				    'adapter' => Core_Cache::ADAPTER_NULL
				)
			)
		);