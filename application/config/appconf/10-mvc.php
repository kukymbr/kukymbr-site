<?php

return array(

	// MVC params
	'mvc' => array(

		// Remove double spaces, new lines and tabs from output
		'outputRemoveSpaces' => true,

		// Remove html comments from output (<!-- comment -->)
		'outputRemoveComments' => false,
	),
);