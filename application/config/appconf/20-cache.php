<?php

return array(

	// Cache configuration sets
	'cache' => array(

		// Default cache instance
		Core_App::DEFAULT_CACHE_NAME => array(

			// Cache adapter name
			'adapter' => Core_Cache::ADAPTER_APCU,

			// Alternative adapter, if main is not available
			'alternativeAdapter' => Core_Cache::ADAPTER_FILE,

			// Cache storage path for file adapter
			'path' => DataStorage::getInstance()->dir('cache') . Core_App::DEFAULT_CACHE_NAME,

			// Cache lifetime
			'lifetime' => 1800,
		),
	),
);