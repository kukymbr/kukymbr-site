<?php

/**
 * Application config array
 * @return array
 */
return array(

	// App environment mode (develop/production)
	'environment' => 'develop',

	// Default timezone
	'timezone' => 'Europe/Moscow',

	// Application params (Optional section)
	'app' => array(

		// Application name
		'name' => 'Sergey Basov, web developer',
	),
);