<?php

/**
 * Main (default) controller
 */
class MainController extends Core_Controller
{
	/**
	 * Index page
	 */
	public function indexAction()
	{

	}

	/**
	 * Undefined method exceptions
	 * Override __call() method in child controller to
	 * process undefined actions and methods
	 *
	 * @param $method
	 * @param $arguments
	 */
	public function __call($method, $arguments)
	{
		$this->_error404();
	}
}