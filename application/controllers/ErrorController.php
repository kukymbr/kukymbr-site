<?php

/**
 * HTTP errors controller
 */
class ErrorController extends Core_Controller_Default_Error
{
	/**
	 * Pre-dispatch actions
	 * Called before action method.
	 * It may modify request to skip current action processing
	 *
	 * @return void
	 */
	public function preDispatch()
	{
		$this->view->headItems[] = 'error.css';
	}
}