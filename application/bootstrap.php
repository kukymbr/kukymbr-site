<?php

// Path to Core_App file
require_once __DIR__ . '/../library/core/App.php';

$app = Core_App::getInstance();

$app->setApplicationPath(__DIR__);
$app->bootstrap();