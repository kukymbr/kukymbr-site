# data/ directory

Directory for auto-generated files.
It is highly recommended to create and access subdirectories and files in this directory only through Core_DataStorage methods.

    @see Core_DataStorage
    @see Core_DataStorage_Default
    @see DataStorage model in application example